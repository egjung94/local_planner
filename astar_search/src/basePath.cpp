
#include "astar_search/astar_search.h"
#include "custom_msgs/Lane.h"
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */

geometry_msgs::TwistStamped current_velocity_;
nav_msgs::Path path;

ros::Publisher base_waypoints_pub_;
ros::Publisher velocity_pub_;
ros::Publisher current_pos_pub_;
ros::Publisher closest_waypoint_pub_;

void globalPathCallback(const nav_msgs::Path& msg)
{
  path = msg;

  custom_msgs::Lane lane_msg;

  lane_msg.header = path.header; 
  lane_msg.increment = 1; 
  lane_msg.lane_id = 0;
  lane_msg.lane_index = 0;
  lane_msg.cost = 0;
  lane_msg.closest_object_distance = 0;
  lane_msg.closest_object_velocity = 0;
  lane_msg.is_blocked = false;

  for(int i = 0 ; i < path.poses.size() ; i++)
  {
    custom_msgs::Waypoint wp;

    wp.pose.pose.position.x = path.poses.at(i).pose.position.x;
    wp.pose.pose.position.y = path.poses.at(i).pose.position.y;
    wp.pose.pose.position.z = path.poses.at(i).pose.position.z;
    wp.pose.pose.orientation = path.poses.at(i).pose.orientation;

    wp.twist.twist.linear.x = 1; //path.at(i).v;
    wp.lane_id = 0; //path.at(i).laneId;
    wp.stop_line_id = -1; //path.at(i).stopLineID;
    wp.left_lane_id = -1; //path.at(i).LeftPointId;
    wp.right_lane_id = -1; //path.at(i).RightPointId;
    wp.time_cost = 0; //path.at(i).timeCost;

    wp.gid = i; //path.at(i).gid;

    wp.cost = 0;

    lane_msg.waypoints.push_back(wp);
  }
  ROS_INFO("base_waypoints size: [%d]",lane_msg.waypoints.size());

  base_waypoints_pub_.publish(lane_msg);
}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "globalPathGenerator");

  ros::NodeHandle nh_;

  ros::Subscriber global_path_sub_ = nh_.subscribe("global_path", 1, globalPathCallback);

  // Temp
  base_waypoints_pub_ = nh_.advertise<custom_msgs::Lane>("base_waypoints", 1, true);
  velocity_pub_ = nh_.advertise<geometry_msgs::TwistStamped>("current_velocity", 1, true);
  current_pos_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("current_pose", 1);
  closest_waypoint_pub_ = nh_.advertise<std_msgs::Int32>("closest_waypoint", 1);

  ros::Rate loop_rate(10);

  int count = 0;

  while (ros::ok())
  {

    ROS_INFO("path_poses size: [%d]", path.poses.size());

    current_velocity_.twist.linear.x = 1.1;

    velocity_pub_.publish(current_velocity_);

    for(int i = 0; i < path.poses.size() ; i++){
      
      /*
      geometry_msgs::PoseStamped ps = path.poses.at(i);

      ps.header.stamp = ros::Time::now();
      ps.header.frame_id = "map";
      current_pos_pub_.publish(ps);*/
      
      //Temp
      std_msgs::Int32 closest_waypoint_index_;
      closest_waypoint_index_.data = static_cast<int32_t>(i);
      closest_waypoint_pub_.publish(closest_waypoint_index_);
      sleep(1);
    }

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  return 0;
}