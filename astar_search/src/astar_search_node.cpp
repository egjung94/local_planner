
#include "astar_search/astar_search.h"
using namespace std;


tf::Transform local2costmap_;  // local frame (e.g. velodyne) -> costmap origin

nav_msgs::OccupancyGrid global_cmap_;
geometry_msgs::PoseStamped start_pose_local_;
geometry_msgs::PoseStamped goal_pose_local_;
geometry_msgs::PoseStamped start_pose_global_;
geometry_msgs::PoseStamped goal_pose_global_;

void costmapCallback(const nav_msgs::OccupancyGrid& msg)
{
  global_cmap_ = msg;
  //tf::poseMsgToTF(global_cmap_.info.origin, local2costmap_);
}

void startPoseCallback(const geometry_msgs::PoseStamped& msg)
{
  start_pose_local_ = msg;
	
  // start_pose_local_.pose = transformPose(
  // start_pose_global_.pose, getTransform(global_cmap_.header.frame_id,  start_pose_global_.header.frame_id));
  // start_pose_local_.header.frame_id = global_cmap.header.frame_id;
  // start_pose_local_.header.stamp = current_pose_global_.header.stamp;

}

void goalPoseCallback(const geometry_msgs::PoseStamped& msg)
{
  goal_pose_local_ = msg;
	
  // goal_pose_local_.pose = transformPose(
  // goal_pose_global_.pose, getTransform(global_cmap_.header.frame_id,  goal_pose_global_.header.frame_id));
  // goal_pose_local_.header.frame_id = global_cmap_.header.frame_id;
  // goal_pose_local_.header.stamp = goal_pose_global_.header.stamp;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "astar_search");
  
  ros::NodeHandle nh;

  ros::Subscriber costmap_sub_ = nh.subscribe("world", 1, costmapCallback);
  ros::Subscriber start_pose_sub_ = nh.subscribe("start_pose", 1, startPoseCallback);
  ros::Subscriber goal_pose_sub_ = nh.subscribe("goal_pose", 1, goalPoseCallback);

  static ros::Publisher gPath_pub = nh.advertise<nav_msgs::Path>("global_path", 1, true);
  
	ros::Rate loop_rate(10);

  AstarSearch node;
  	
  bool found_path = false;
  
  
  // set goal pose
  // goal_pose_global_ = avoid_waypoints_.waypoints[goal_waypoint_index].pose;
  //goal_pose_local_.pose = transformPose(goal_pose_global_.pose, getTransform(global_cmap_.header.frame_id, goal_pose_global_.header.frame_id));
 
  
	while(ros::ok()){

		ROS_INFO_STREAM("global_cmap_:" << endl << global_cmap_.info.resolution << ", " << global_cmap_.info.width << ", " << global_cmap_.info.height);

	  //node.initialize(global_cmap_);

    ROS_INFO("start_pose: %f, %f ; goal_pose: %f, %f", start_pose_local_.pose.position.x, start_pose_local_.pose.position.y, goal_pose_local_.pose.position.x, goal_pose_local_.pose.position.y);

    //found_path = node.makePlan(start_pose_local_.pose, goal_pose_local_.pose);
    
		cout << "found_path: " << found_path << endl;

    // Path made by me
    nav_msgs::Path my_path_; 
    std_msgs::Header header;
    header.stamp = ros::Time::now();
    header.frame_id = global_cmap_.header.frame_id;
    my_path_.header = header;

    for (int i = 0; i < 100; i++){
      geometry_msgs::PoseStamped ps;
      ps.header = header;
      ps.pose.position.x = i*0.1;
      ps.pose.position.y = 0;
      ps.pose.orientation.w = 1; 
      my_path_.poses.push_back(ps);
    }

    gPath_pub.publish(my_path_);

    //

    if (found_path)
    {
      //gPath_pub.publish(node.getPath());

      /*
      ROS_INFO("Found Path and it is shown below\n"); //node.getPath.poses[]
      for (int i = 0 ; i < (node.getPath().poses).size() ; i++)
      {
        ROS_INFO("\n-----index : %d-------",i);
        ROS_INFO("position x is %f",node.getPath().poses[i].pose.position.x);
        ROS_INFO("position y is %f",node.getPath().poses[i].pose.position.y);
        ROS_INFO("position z is %f",node.getPath().poses[i].pose.position.z);
        ROS_INFO("orientation x is %f",node.getPath().poses[i].pose.orientation.x);
        ROS_INFO("orientation y is %f",node.getPath().poses[i].pose.orientation.y);
        ROS_INFO("orientation z is %f",node.getPath().poses[i].pose.orientation.z);
        ROS_INFO("orientation w is %f",node.getPath().poses[i].pose.orientation.w);
      }*/
      
      node.reset();
      // break;
    }
    
   	ros::spinOnce();
   	
   	loop_rate.sleep();
  }

  
  return 0;
}

