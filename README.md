# project local_planner 
## Made by Epic and grandoba using Autoware AI

# .bashrc codes

```sh
alias no1_loadmap="rosrun map_gen map_gen_node"
alias no2_generateCostmap="roslaunch ~/catkin_ws/src/local_planner/costmap_generator/launch/costmap_generator.launch replay:=true"
alias no3_astar_search="rosrun astar_search astar_search_exe"
alias no4_gloablPathGenerator="rosrun astar_search globalPathGenerator_exe"
alias no5_astar_avoid="roslaunch ~/catkin_ws/src/local_planner/local_planning/launch/astar_avoid.launch replay:=true"
alias no6_velocity_set="roslaunch ~/catkin_ws/src/local_planner/local_planning/launch/velocity_set.launch replay:=true"
```
