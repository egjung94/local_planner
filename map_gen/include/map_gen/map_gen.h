#ifndef MAP_GEN_H
#define MAP_GEN_H

#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseStamped.h>

class MapGen
{
public:
  MapGen();
  ~MapGen();
	
	nav_msgs::OccupancyGrid global_cmap_;
  
  const nav_msgs::OccupancyGrid& getGlobalMap() 
  {
    return global_cmap_;
  }

};


#endif 