
#include "map_gen/map_gen.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_gen");
  
  ros::NodeHandle nh;

  static ros::Publisher pub_global_cmap_ = nh.advertise<nav_msgs::OccupancyGrid>("world", 1, true);
  
	ros::Rate loop_rate(10);

  MapGen node;
  
  
	while(ros::ok()){

    pub_global_cmap_.publish(node.getGlobalMap());
    
   	ros::spinOnce();
   	
   	loop_rate.sleep();
  }

  
  return 0;
}