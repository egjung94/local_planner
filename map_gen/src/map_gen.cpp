
#include "map_gen/map_gen.h"
using namespace std;



MapGen::MapGen(){

  /* Testing global map */

  // static ros::Publisher pub_global_cmap_ = nh.advertise<nav_msgs::OccupancyGrid>("world", 1, true);

  global_cmap_.header.seq = 1;
  global_cmap_.header.stamp = ros::Time::now();
  global_cmap_.header.frame_id = "odom";

  global_cmap_.info.map_load_time = global_cmap_.header.stamp;
  global_cmap_.info.resolution = 1;
  global_cmap_.info.width = 40;
  global_cmap_.info.height = 40;

  // Costmap origin set to be the centre of the grid (as in points2costmap.cpp)
  global_cmap_.info.origin.position.x = -global_cmap_.info.resolution*global_cmap_.info.width/2;
  global_cmap_.info.origin.position.y = -global_cmap_.info.resolution*global_cmap_.info.height/2;
  global_cmap_.info.origin.position.z = 0;
  global_cmap_.info.origin.orientation.x = 0;
  global_cmap_.info.origin.orientation.y = 0;
  global_cmap_.info.origin.orientation.z = 0;
  global_cmap_.info.origin.orientation.w = 1;

  for (int idx = 0; idx < global_cmap_.info.width*global_cmap_.info.height; ++idx)
  {
    global_cmap_.data.push_back(0);
  }

  ros::NodeHandle nh("~");
  nh.setParam("lateral_goal_range", 1.5);
}


MapGen::~MapGen()
{
}
  